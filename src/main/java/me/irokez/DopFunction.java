package me.irokez;

import java.util.regex.Pattern;

public class DopFunction {

    public static String connectBootstrap() {
        return "<link href=\"../css/bootstrap.css\" rel=\"stylesheet\">\n";
    }

    public static String answerFromServlet(String backPage, String message) {
        return ("<link href=\"../css/bootstrap.css\" rel=\"stylesheet\">\n" +
                message + "\n" +
                "<br><br><a href=\"/task" + backPage + "\">Назад</a>\n" +
                "<br><br><a href=\"/index.jsp\">На главную</a>");
    }

    public static boolean testingToFilling(String... strings) {
        boolean bool = false;
        for (String s : strings) {
            bool = s.matches("");
        }
        return bool;
    }

    public static boolean testingToNumeric(String... strings) {
        boolean bool = false;
        for (String s : strings) {
            bool = !Pattern.matches("[0-9]+", s);
        }
        return bool;
    }
}