package me.irokez.task1;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task1/task1_fifth")
public class FifthNameShorting extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String backPage = "1/fifthNameShorting.jsp";
        String name = request.getParameter("name");
        String lastName = request.getParameter("lastName");
        String fatherName = request.getParameter("fatherName");

        try {
            if (DopFunction.testingToFilling(name, lastName, fatherName)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (!DopFunction.testingToNumeric(name, lastName, fatherName)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Вводите правильные данные"));
            } else {
                writer.println(DopFunction.answerFromServlet(backPage, "Укороченное ФИО: " + lastName.toUpperCase()
                        + " " + name.toLowerCase().charAt(0) + ". " + fatherName.toLowerCase().charAt(0) + "."));
            }
        } finally {
            writer.close();
        }
    }
}