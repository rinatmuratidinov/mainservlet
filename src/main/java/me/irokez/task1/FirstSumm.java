package me.irokez.task1;
import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task1/task1_first")
public class FirstSumm extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String number = request.getParameter("numbers");
        String backPage = "1/firstSumm.jsp";

        try {
            if (DopFunction.testingToFilling(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else {
                int sum = 0;
                for (int i = 0; i < number.length(); i++) {
                    sum += Integer.parseInt(number.substring(i, i+1));
                }
                writer.println(DopFunction.answerFromServlet(backPage, ("Сумма введённых чисел: " + sum)));
            }
        } finally {
            writer.close();
        }
    }
}
