package me.irokez.task1;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task1/task1_fourth")
public class FourthMinMaxNumbers extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String number = request.getParameter("number");
        String backPage = "1/fourthMinMaxNumbers.jsp";

        try {
            if (DopFunction.testingToFilling(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else {
                writer.println(DopFunction.connectBootstrap());

                int num = Integer.parseInt(number);
                int[] mass = new int[num];
                int lim = 100, min = lim, max = 0;

                writer.println("Наш массив, заполненный случайными числами:<br>");
                for (int i = 0; i < mass.length; i++) {
                    mass[i] = (int) (Math.random() * lim);
                    writer.println(mass[i]);
                }

                for (int i = 0; i < mass.length; i++) {
                    if (mass[i] < min) {
                        min = mass[i];
                    }

                    if (mass[i] > max) {
                        max = mass[i];
                    }
                }

                writer.println("<br>Максимальное число: " + max + "<br>");
                writer.println("Минимальное число: " + min + "<br>");

                for (int i = 0; i < mass.length; i++) {
                    if (mass[i] == min) {
                        mass[i] = max;
                        continue;
                    }

                    if (mass[i] == max) {
                        mass[i] = min;
                    }
                }
                writer.println("Наш массив после изменения:<br>");
                for (int i = 0; i < mass.length; i++) {
                    writer.println(mass[i]);
                }
                writer.println(DopFunction.answerFromServlet(backPage, ""));
            }
        } finally {
            writer.close();
        }
    }
}