package me.irokez.task1;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task1/task1_second")
public class SecondCountRepeats extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String backPage = "1/secondCountRepeats.jsp";
        String numbers = request.getParameter("numbers");
        String numberToCompare = request.getParameter("numberToCompare");

        try {
            if(DopFunction.testingToFilling(backPage, numbers, numberToCompare)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            }
             else if (DopFunction.testingToNumeric(numbers, numberToCompare)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else {
                int counter = 0;
                int sizeOfCompareNumber = numberToCompare.length();
                for (int i = 0; i < numbers.length() - (sizeOfCompareNumber-1); i++) {
                    if (numbers.substring(i, i+sizeOfCompareNumber).matches(numberToCompare)) counter++;
                }
                writer.println(DopFunction.answerFromServlet(backPage, "Число " + numberToCompare + " повторяется " + counter + " раз(а)"));
            }
        } finally {
            writer.close();
        }
    }
}