package me.irokez.task1;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task1/task1_sixth")
public class SixthGradToHour extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String backPage = "1/sixthGradToHour.jsp";
        String gradsField = request.getParameter("grads");

        try {
            if (DopFunction.testingToFilling(gradsField)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(gradsField)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else if (Integer.parseInt(gradsField) > 360 || Integer.parseInt(gradsField) < 0) {
                writer.println(DopFunction.answerFromServlet(backPage, "Диапазон чисел 0 - 360"));
            } else {
                int grads = Integer.parseInt(gradsField);
                int seconds, hours, minutes, ostMin;
                seconds = grads * 120;
                minutes = seconds / 60;
                ostMin = minutes % 60;
                hours = minutes / 60;
                writer.println(DopFunction.answerFromServlet(backPage, grads + " град. = " + hours + " ч. " + ostMin + " м. "));

            }
        } finally {
            writer.close();
        }
    }
}