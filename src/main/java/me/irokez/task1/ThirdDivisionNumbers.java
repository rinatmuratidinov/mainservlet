package me.irokez.task1;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task1/task1_third")
public class ThirdDivisionNumbers extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String backPage = "1/thirdDivisionToNumber.jsp";
        String number1 = request.getParameter("number1");
        String number2 = request.getParameter("number2");
        String numberToDivision = request.getParameter("numberToDivision");

        try {
            if (DopFunction.testingToFilling(number1, number2, numberToDivision)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(number1, number2, numberToDivision)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else {
                writer.println(DopFunction.connectBootstrap());

                int num1 = Integer.parseInt(number1);
                int num2 = Integer.parseInt(number2);
                int num3 = Integer.parseInt(numberToDivision);

                writer.println("Номера, которые делятся на " + numberToDivision + ":");
                for (int i = num1; i <= num2; i++) {
                    if (i % num3 == 0) {
                        writer.println("<br>" + i);
                    }
                }
                writer.println(DopFunction.answerFromServlet(backPage,""));
            }
        } finally {
            writer.close();
        }
    }
}