package me.irokez.task2;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/task2/task2_fifth")
public class FifthDateDifferent extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String backPage = "2/fifthDateDifferent.jsp";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String date1 = request.getParameter("date1");
        String date2 = request.getParameter("date2");

        Map<Integer, String> yearsWords = new HashMap<Integer, String>();
        yearsWords.put(1, "год");
        yearsWords.put(2, "года");
        yearsWords.put(5, "лет");

        Map<Integer, String> monthsWords = new HashMap<Integer, String>();
        monthsWords.put(1, "месяц");
        monthsWords.put(2, "месяца");
        monthsWords.put(5, "месяцев");

        Map<Integer, String> daysWords = new HashMap<Integer, String>();
        daysWords.put(1, "день");
        daysWords.put(2, "дня");
        daysWords.put(5, "дней");

        Map<Integer, String> hoursWords = new HashMap<Integer, String>();
        hoursWords.put(1, "час");
        hoursWords.put(2, "часа");
        hoursWords.put(5, "часов");

        Map<Integer, String> minutesWords = new HashMap<Integer, String>();
        minutesWords.put(1, "минута");
        minutesWords.put(2, "минуты");
        minutesWords.put(5, "минут");

        Map<Integer, String> secondsWords = new HashMap<Integer, String>();
        secondsWords.put(1, "секунда");
        secondsWords.put(2, "секунды");
        secondsWords.put(5, "секунд");

        try {
            if (DopFunction.testingToFilling(date1, date2)) {
                DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля");
            } else {
                Date newDate1 = simpleDateFormat.parse(date1);
                Date newDate2 = simpleDateFormat.parse(date2);
                long days = diffDays(newDate1, newDate2);
                int year = (int) days / 365;
                int month = (int) days / 30;
                int hour = (int) days * 24;
                int minutes = (int) days * 24 * 60;
                int seconds = (int) days * 24 * 60 * 60;
                String yearsWord = FourthEndingOfWord.okonchanie(year, yearsWords);
                String monthsWord = FourthEndingOfWord.okonchanie(month, monthsWords);
                String daysWord = FourthEndingOfWord.okonchanie((int) days, daysWords);
                String hoursWord = FourthEndingOfWord.okonchanie(hour, hoursWords);
                String minutesWord = FourthEndingOfWord.okonchanie(minutes, minutesWords);
                String secondsWord = FourthEndingOfWord.okonchanie(seconds, secondsWords);

                writer.println(DopFunction.answerFromServlet(backPage, "Разница между<br>" + date1 +
                        " -----> " + date2 + "<br><br>" + year + " " + yearsWord + "<br><br> или " + month + " " + monthsWord + "<br><br> или " + days + " " + daysWord
                        + "<br><br> или " + hour + " " + hoursWord + "<br><br> или " + minutes + " " + minutesWord + "<br><br> или " + seconds + " " + secondsWord));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }

    public long diffDays(Date one, Date two) {
        long delta = (two.getTime() - one.getTime()) / 86400000;
        return Math.abs(delta);
    }
}