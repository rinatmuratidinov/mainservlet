package me.irokez.task2;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task2/task2_first")
public class FirstTrafficLight extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String number = request.getParameter("number");
        String backPage = "2/firstTrafficLight.jsp";

        try {
            if (DopFunction.testingToFilling(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else {
                int minutes = Integer.parseInt(number);
                String colorOfTrafficLight;
                int ostatok = minutes % 5;
                if (ostatok <= 3 && ostatok != 0) {
                    colorOfTrafficLight = "Зелёный";
                } else {
                    colorOfTrafficLight = "Красный";
                }
                writer.println(DopFunction.answerFromServlet(backPage, colorOfTrafficLight));
            }
        } finally {
            writer.close();
        }
    }
}