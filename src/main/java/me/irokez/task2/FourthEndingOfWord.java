package me.irokez.task2;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task2/task2_fourth")
public class FourthEndingOfWord extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String number = request.getParameter("number");
        String backPage = "2/fourthEndingOfWord.jsp";
        Map<Integer, String> words = new HashMap<Integer, String>();
        words.put(1, "Стол");
        words.put(2, "Стола");
        words.put(5, "Столов");

        try {

            if (DopFunction.testingToFilling(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else {
                int chislo = Integer.parseInt(number);
                String result = okonchanie(chislo, words);
                writer.println(DopFunction.answerFromServlet(backPage, result));
            }
        } finally {
            writer.close();
        }
    }

    public static String okonchanie(int chislo, Map<Integer, String> words) {
        String result;
        if (chislo > 110 && chislo % 10 == 1 && chislo < 1000) {
            result = words.get(5);
        } else if (chislo % 10 >= 5 && chislo % 5 == 0 || chislo % 100 > 10 && chislo % 100 <= 20) {
            result = words.get(5);
        } else if (chislo % 10 == 1) {
            result = words.get(1);
        } else if (chislo % 10 > 1 && chislo % 10 < 5) {
            result = words.get(2);
        } else {
            result = words.get(5);
        }
        return result;
    }
}