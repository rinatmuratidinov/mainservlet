package me.irokez.task2;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task2/task2_second")
public class SecondYearsWith29February extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String number = request.getParameter("number");
        String backPage = "2/secondYearsWith29February.jsp";


        try {
            if (DopFunction.testingToFilling(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else if (Integer.parseInt(number) < 0 || Integer.parseInt(number) > 9999) {
                writer.println(DopFunction.answerFromServlet(backPage, "Вводить можно только цифры в диапазоне 1 - 9999!"));
            } else {
                int god = Integer.parseInt(number);
                String result;
                if (god % 4 == 0) {
                    result = "Високосный год";
                } else {
                    result = "Не високосный год";
                }
                writer.println(DopFunction.answerFromServlet(backPage, result));
            }
        } finally {
            writer.close();
        }
    }
}