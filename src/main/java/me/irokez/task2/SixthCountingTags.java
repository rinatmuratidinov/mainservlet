package me.irokez.task2;

import me.irokez.DopFunction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

@WebServlet("/task2/task2_sixth")
public class SixthCountingTags extends HttpServlet {

    private static final String URL = "jdbc:mysql://localhost:3306/site_tags"
            + "?verifyServerCertificate=false"
            + "&useSSL=false"
            + "&requireSSL=false"
            + "&useLegacyDatetimeCode=false"
            + "&amp"
            + "&serverTimezone=UTC";

    private static final String USER = "root";
    private static final String PASS = "root";


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String backPage = "2/sixthCountingTags.jsp";

        String siteName = request.getParameter("site");
        String tagName = request.getParameter("tag");

        try {
            DopFunction.connectBootstrap();


            if (DopFunction.testingToFilling(siteName, tagName)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Поля не могут быть пустыми"));
            } else {
                int tagCount = countOfTags(siteName, tagName);
                Connection connection;
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(URL, USER, PASS);
                Statement statement = connection.createStatement();
                int count = 0;
                ResultSet del = statement.executeQuery("select count(*) from sites where site = '" + siteName + "' and tag = '" + tagName + "'");
                while (del.next()) {
                    count = del.getInt("count(*)");
                }
                String selectQuery = "select * from sites where site = '" + siteName + "' and tag = '" + tagName + "'";

                if (count < 1) {
                    statement.execute("insert into sites(site, tag, tag_count) values('" + siteName + "', '" + tagName + "', " + tagCount + ")");
                }
                    getDb(writer, statement.executeQuery(selectQuery));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }

    public static int countOfTags(String site, String tag) {

        System.setProperty("webdriver.chrome.driver", "D:\\Download\\chromedriver_win32\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get("https://www." + site);
        List<WebElement> listwebelement = driver.findElements(By.tagName(tag));
        driver.close();
        return listwebelement.size();
    }

    public static void getDb(PrintWriter writer, ResultSet rs) throws SQLException {
        DopFunction.connectBootstrap();
        writer.println("<table border=\"1\" style=\"margin: 0 auto;\">" +
                "<caption>Таблица тегов</caption>" +
                "<tr>" +
                "<th>Номер</th>" +
                "<th>Сайт</th>" +
                "<th>Тег</th>" +
                "<th>Количество</th>" +
                "</tr>"
        );
        while (rs.next()) {
            int id = rs.getInt("id");
            String site = rs.getString("site");
            String tag = rs.getString("tag");
            int tagcount = rs.getInt("tag_count");
            writer.println("<tr><td>" + id + "</td><td>" + site + "</td><td>" + tag + "</td><td>" + tagcount + "</td></tr>");
        }
        writer.println("</table >");
    }
}
