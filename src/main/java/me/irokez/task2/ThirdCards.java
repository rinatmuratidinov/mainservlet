package me.irokez.task2;

import me.irokez.DopFunction;

import java.io.PrintWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/task2/task2_third")
public class ThirdCards extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter writer = response.getWriter();
        String number = request.getParameter("number");
        String backPage = "2/thirdCards.jsp";
        Map<Integer, String> cards = new HashMap<Integer, String>();
        cards.put(1, "Единица");
        cards.put(2, "Двойка");
        cards.put(3, "Тройка");
        cards.put(4, "Четвёрка");
        cards.put(5, "Пятёрка");
        cards.put(6, "Шестёрка");
        cards.put(7, "Семёрка");
        cards.put(8, "Восьмёрка");
        cards.put(9, "Девятка");
        cards.put(10, "Десятка");
        cards.put(11, "Валет");
        cards.put(12, "Дама");
        cards.put(13, "Король");
        cards.put(14, "Туз");

        try {

            if (DopFunction.testingToFilling(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно заполнить все поля"));
            } else if (DopFunction.testingToNumeric(number)) {
                writer.println(DopFunction.answerFromServlet(backPage, "Нужно вводить номер"));
            } else if (Integer.parseInt(number) < 0 || Integer.parseInt(number) > 14) {
                writer.println(DopFunction.answerFromServlet(backPage, "Вводить можно только цифры в диапазоне 1 - 14"));
            } else {
                int chislo = Integer.parseInt(number);
                writer.println(DopFunction.answerFromServlet(backPage, cards.get(chislo)));
            }
        } finally {
            writer.close();
        }
    }
}