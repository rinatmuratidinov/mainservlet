<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Пятая</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task1_fifth" method="POST">
    <div align="">
        Имя: <input meta charset="UTF-8" name="name" type="text" required/>
        <br><br>
        Фамилия: <input meta charset="UTF-8" name="lastName" type="text" required/>
        <br><br>
        Отчество: <input meta charset="UTF-8" name="fatherName" type="text" required/>
        <br><br>
    </div>
    <input meta charset="UTF-8" type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task1/task1.jsp">Задания 1</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>
