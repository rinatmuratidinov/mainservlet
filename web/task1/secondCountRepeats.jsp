<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Вторая</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task1_second" method="POST">
    Номеры: <input name="numbers" type="number" required/>
    <br><br>
    Номер, который нужно подсчитать: <input name="numberToCompare"  type="number" required/>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task1/task1.jsp">Задания 1</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>