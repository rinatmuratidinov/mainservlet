<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.12.2018
  Time: 10:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Задания 1</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="" method="POST">
    <a href="/task1/firstSumm.jsp">1) Нахождение суммы введённых номеров</a>
    <br><br>
    <a href="/task1/secondCountRepeats.jsp">2) Подсчитывание количества определённого числа в наборе из чисел</a>
    <br><br>
    <a href="/task1/thirdDivisionToNumber.jsp">3) Нахождение делителей числа</a>
    <br><br>
    <a href="/task1/fourthMinMaxNumbers.jsp">
        4) Создание массива, заполнение случайными числами,<br>
        нахождение максимального и минимального значение, <br>
            поменять их и вывести массив
        </a>
    <br><br>
    <a href="/task1/fifthNameShorting.jsp">5) Вывод укороченной версии введённой ФИО</a>
    <br><br>
    <a href="/task1/sixthGradToHour.jsp">6) Перевод градусов в часы</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>