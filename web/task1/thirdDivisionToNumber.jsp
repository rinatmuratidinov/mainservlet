<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Третья</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task1_third" method="POST">
    От номера: <input name="number1" type="number" required/>
    <br><br>
    До номера: <input name="number2" type="number" required/>
    <br><br>
    Номер, на который должно делиться: <input name="numberToDivision" type="number" required/>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task1/task1.jsp">Задания 1</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>
