<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Пятая</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task2_fifth" method="POST">
    Программа выводит разницу между двумя датами.
    <br><br>
    Введите 1 - дату: <input type="date" name="date1" required>
    <br><br>
    Введите 2 - дату: <input type="date" name="date2" required/>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task2/task2.jsp">Задания 2</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>