<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Первая</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task2_first" method="POST">
    С запуска светофора, первые 3 минуты он горит зелёным а потом 2 минуты красным.<br>
    Вы вводите N-минуту, а программа определит, какой свет сейчас горит.<br><br>
    Введите число: <input name="number" type="number" required/>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task2/task2.jsp">Задания 2</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>