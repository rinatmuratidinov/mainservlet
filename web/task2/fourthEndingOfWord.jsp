<%@ page import="me.irokez.task2.FourthEndingOfWord" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Четвёртая</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task2_fourth" method="POST">
    Программа выводит множественную форму слова "стол" в зависимости от введённого вами числа.
    <br><br>
    Введите число: <input name="number" type="number" required/>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task2/task2.jsp">Задания 2</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>