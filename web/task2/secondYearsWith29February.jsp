<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Вторая</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task2_second" method="POST">
    В году чуть больше чем 365 дней (прмерно на 6 часов). И чтобы накопившийся раз в 4 года день не пропал,<br>
    её добавляют как 29 февраля и называют этот год високосным. Это программа подсчитает,<br>
    является ли год, который Вы ввели, високосным.<br><br>
    Введите число: <input name="number" type="number" required/>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task2/task2.jsp">Задания 2</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>