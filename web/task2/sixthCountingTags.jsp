<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Шестая</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task2_sixth" method="POST">
    Программа выводит количество тегов на сайте.
    <br><br>
    Введите сайт: www.<input type="text" name="site" required/>
    <br><br>
    Введите тег: <<input type="text" name="tag" required/>>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task2/task2.jsp">Задания 2</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
<form action="sites" method="post">
    <input type="submit" value="Все результаты"/>
</form>
</body>
</html>