<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Задания 2</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="" method="POST">
    <a href="/task2/firstTrafficLight.jsp">1) Угадай цвет светофора</a>
    <br><br>
    <a href="/task2/secondYearsWith29February.jsp">2) Угадывание на високосный год</a>
    <br><br>
    <a href="/task2/thirdCards.jsp">3) Название карты из его номера</a>
    <br><br>
    <a href="/task2/fourthEndingOfWord.jsp">4) Окончание слова в зависимости от числа</a>
    <br><br>
    <a href="/task2/fifthDateDifferent.jsp">5) Разница между 2 датами</a>
    <br><br>
    <a href="/task2/sixthCountingTags.jsp">6) Подсчёт количества тегов на сайте</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>