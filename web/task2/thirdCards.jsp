<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Третья</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
</head>
<body>
<form action="task2_third" method="POST">
    Программа выводит название карты от введённого числа. Номер должен быть в диапазоне 1 - 14.
    <br><br>
    Введите число: <input name="number" type="number" required/>
    <br><br>
    <input type="submit" value="Подтвердить"/>
    <br><br>
    <a href="/task2/task2.jsp">Задания 2</a>
    <br><br>
    <a href="/index.jsp">На главную</a>
</form>
</body>
</html>